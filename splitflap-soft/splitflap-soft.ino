#include <SoftwareSerial.h>

#define fadePin 10
#define brakePin 2
#define fadePinPlus 12
SoftwareSerial mySerial(2, 4); // RX, TX

#define encoderPin 3

int countSeekingZero = 0;
int count = 0;
int lastStateRoueCodeuse = 0;
unsigned long lastTick;
unsigned long tick;

float previousTickTime = 0;
float tickTime = 0;
int averageTick = 0;

int state = 100;

int target = 10;
int newTarget;

int toleranceZero = 3; // marge d'erreur quand on estime le zero


unsigned long waitingStartTime = 0;
int capteurStateWhenWaiting;

volatile byte theTick = LOW;
byte oldTick = LOW;
volatile long tempcount = 0;
boolean rising = false;
boolean tickDone = false;


void setup() {
  Serial.begin(115200);
  pinMode(fadePin, OUTPUT);
  pinMode(brakePin, OUTPUT);
  pinMode(fadePinPlus, OUTPUT);

  digitalWrite(fadePin, LOW);
  digitalWrite(brakePin, HIGH);
  digitalWrite(fadePinPlus, LOW);

  //DIP

  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);


  //Optique
  pinMode(encoderPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(encoderPin), tickUpdate, CHANGE);

  mySerial.begin(57600);

  Serial.print("My ID: ");

  Serial.print(digitalRead(4));
  Serial.print(digitalRead(5));
  Serial.print(digitalRead(6));
  Serial.print(digitalRead(7));
  Serial.print(digitalRead(8));
  Serial.print(digitalRead(9));
  Serial.println(digitalRead(11));

}

void motorSeek()
{
  analogWrite(fadePin, 127);
  digitalWrite(brakePin, HIGH);
  digitalWrite(fadePinPlus, LOW);

}

void motorSeekSlow()
{
  analogWrite(fadePin, 60);
  digitalWrite(brakePin, HIGH);
  digitalWrite(fadePinPlus, LOW);
  
}

void brake()
{
  // Un delay qui permet de s'arrêter entre deux crans
  //delay(90);
  digitalWrite(fadePin, LOW);
  digitalWrite(fadePinPlus, HIGH);
  delay(15);
  digitalWrite(fadePin, HIGH);
  digitalWrite(brakePin, LOW);
  delay(100);
  digitalWrite(fadePin, LOW);
  digitalWrite(brakePin, HIGH);
  digitalWrite(fadePinPlus, LOW);
  delay(500);
}

void loop() {
  motorSeek();
  tempcount = (tempcount/3)*2;
  //if (tempcount>0) Serial.println(tempcount);

  if (tempcount>0 && rising == false && tickDone == false)
  {
    rising = true;
    //Serial.println(tempcount);
  }
  if (tempcount>100 && rising == true)
  {
    //Serial.println("tic");
    tick = millis();
    tickTime = tick - lastTick;
    //Serial.println(tickTime);
        
    rising = false;
    tickDone = true;
    if (tickTime > 40)
    {
      count++;
      Serial.println(tickTime);
    }
      
  }
  
  if (tempcount == 0 && rising == false)
  {
    rising = false;
    tickDone = false;
  }
  lastTick = tick;

  switch (state)
  {
    case 0:
      // On cherche le zero, en calculant le temps entre les intervalles
      motorSeek();

      if (theTick == 1 && lastStateRoueCodeuse == 0)
      {
        tick = millis();
        tickTime = tick - lastTick;
        Serial.println(tickTime);
        countSeekingZero++;
        count = 0;
        // On ne compte pas les 3 premières mesures, car avec l'acceleration progressive on a de faux positifs.
        if (tickTime > previousTickTime * 1.3 && previousTickTime > 120 && countSeekingZero > 3 )
        {
          newTarget = target;
          state = 10;
          
        }
      }

      lastStateRoueCodeuse = theTick;
      lastTick = tick;
      previousTickTime = tickTime;
      break;

    case 10:
      // On cherche la lettre en comptant les intervalles
      // Version progressive
      /*
        if (count < target - 7)
        {
        analogWrite(fadePin, 127);
        } else if (count >= target - 7 && count < target) {

        analogWrite(fadePin, 60);

        }
        else {

        brake();
        capteurStateWhenWaiting = digitalRead(3);
        waitingStartTime = millis();
        tick = millis();
        countSeekingZero = 0;
        state = 20;
        }
      */

      //Version radicale

      if (count < newTarget-2)
      {
        motorSeek();
      } else if (count < newTarget ){
        motorSeekSlow();
      }else{
        brake();
        capteurStateWhenWaiting = theTick;
        waitingStartTime = millis();
        tick = millis();
        countSeekingZero = 0;
        state = 20;
      }

      // Un dépassement semble être un saut de pas
      // Donc, si un pas semble plus long, on soustrait 1 à target
      if (theTick == 1 && lastStateRoueCodeuse == 0)
      {
        tick = millis();
        tickTime = tick - lastTick;
        Serial.print(tickTime);
        Serial.println(" 1 tick");
        if (tickTime > 210)
        {
          //On a sauté un pas
          newTarget --;
          Serial.println(" 1 pas sauté");
        }
        count ++;

      }
      /*
        if (digitalRead(3) == 1 && lastStateRoueCodeuse == 0)
        {
        Serial.println("1 tick");
        //if (tick - lastTick > 280) state = 10;
        count ++;
        }
      */
      lastTick = tick;
      lastStateRoueCodeuse = theTick;
      break;



    case 20:
      //test
      //Serial.println(digitalRead(3));
      if (theTick == 0 && lastStateRoueCodeuse == 1)
      {
        Serial.println("Dépassé");
      }

      // On vérifie que le tambour ne dépasse pas sa position
      if (capteurStateWhenWaiting != digitalRead(3))
      {
        tick = millis();
        countSeekingZero = 0;
        state = 30;
      }

      //Serial.println(millis() - waitingStartTime);
      //temps de pause
      if (millis() - waitingStartTime > 4000 )
      {
        tick = millis();
        countSeekingZero = 0;
        state = 30;
      }
      break;

    case 30:

      // On relance la recherche du zero, en calculant le temps entre les intervalles, mais cette fois en estimant si on est proche du compte, par rapport au target.
      // Par exemple si le target est 10, il doit y avoir environ 40 steps avant le zero

      int estimatedZero = 50 - target;

      motorSeek();

      if (theTick == 1 && lastStateRoueCodeuse == 0)
      {
        tick = millis();
        tickTime = tick - lastTick;
        Serial.println(tickTime);
        countSeekingZero++;
        count = 0;
        // On ne compte pas les 3 premières mesures, car avec l'acceleration progressive on a de faux positifs.
        // Si le nombre de pas semble à peu près correct, on ve charcher notre lettre
        if (tickTime > previousTickTime * 1.4 && previousTickTime > 120 && countSeekingZero > 3 &&  countSeekingZero > estimatedZero - toleranceZero && countSeekingZero < estimatedZero + toleranceZero)
        {
          newTarget = target;
          state = 10;
        }
        // En revanche, le nombre de pas ne correspond pas à ce qu'on devrait avoir, soit la lettre précédente était fausse, soit il a trouvé un faux zéro.
        // On renvoie au state 0 pour refaire une recherche complète
        else if ( countSeekingZero > 65)
        {
          countSeekingZero = 0;
          state = 0;
        }
      }

      lastStateRoueCodeuse = theTick;
      lastTick = tick;
      previousTickTime = tickTime;

      break;



  }

  
  /*
    // Pour les tests, on ne fait tourner que 5 secondes

    if (millis() < 5000)
    {
    analogWrite(fadePin, 127);
    } else if (millis() > 5000 && millis() < 5500) {
    analogWrite(fadePin, 40);
    } else {
    analogWrite(fadePin, 0);
    }
  */


  /*



    if (mySerial.available()) {
    char inChar = (char)mySerial.read();
    Serial.println(inChar);
    if (inChar == 'h')
    {
      analogWrite(fadePin, 255);
      delay(15000);
      Serial.println("STOP1");
      analogWrite(fadePin, 50);
      delay(1000);
      Serial.println("STOP2");
    }
    }else{

    analogWrite(fadePin, 0);
    }
  */
  delay(1);

}

void tickUpdate() {
  //theTick = !theTick;
  tempcount+=100;
}
