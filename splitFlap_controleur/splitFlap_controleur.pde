//TRAME ENVOYEE
//S -> start
//XX -> index de la lettre en tre 0 et 62
//XXX -> caractère
//XX -> correction 1
//XX -> correction 2
//E -> end
// 666 reset
// 333 avance lente


import controlP5.*;
import java.util.*;
import processing.serial.*;

ControlP5 cp5;


int rows = 3;
int cols = 21;
int letterWidth = 50;
int letterHeight = 75;
int interlignage = 15;
char[] message = new char[63];
int index = 0;
int offset;

ArrayList <Letter> theLetters;
PFont f;
PFont f2;

// Pour stocker les correspondances entre caractère et index sur la roue codeuse
IntDict listCharacters;

//??
int[] serialInArray = new int[11];
int serialCount = 0;
short LF = 10; 

//RS485
Serial myPort485;
int myRS485port=-1;
Boolean myPort485Created = false;  
String portName;
// Delai de base pour les envois
int delaiBase = 50;
int nbreDeRetry = 3;

//Stockage des messages
ArrayList <char[]> storedMessages = new ArrayList<char[]>();
int currentIndexMessage = 0;
char[] messageStock= new char[63];
int offsetXStock;
int offsetYStock;
// A JSON object
JSONObject json;

//Stockage des paramètres de chaque caractère
int indexSelected = -1;
int storedCorrectionFactor1 = 0;
int storedCorrectionFactor2 = 0;
int offsetXCaractereSetup;
int offsetYCaractereSetup; 
// A JSON object to store each correction param
JSONArray paramsValues;

Textlabel myTextlabelA;

//Delai d'envoi
int sliderValue = delaiBase;
int multiplicateur = 1;
boolean toggleValue = false;
IntList shuffleIndex;

String lastFileLoaded = "";

void setup()
{
  size(1200, 768);
  background(0);

  f = loadFont("PFDINMono-Bold-48.vlw");
  f2 = loadFont("ProcessingSansPro-Regular-12.vlw");

  listCharacters = new IntDict();
  populateDict();

  //Liste pour organiser les différents splits flaps
  theLetters = new ArrayList <Letter>();

  //décalage pour le placement au centre de l'écran
  offset = (width-(cols*letterWidth))/2;

  //Stock 
  offsetXStock = width/2-165;
  offsetYStock = height-175;

  // Au départ on remplit avec des espaces
  for (int i = 0; i<rows*cols; i++)
  {
    message[i] = ' ';
  }

  // génération des flaps
  for ( int y=0; y<rows; y++)
  {
    for (int x=0; x<cols; x++)
    {
      Letter myLetter = new Letter(x+(y*cols), x*letterWidth+offset, y*letterHeight+(interlignage*y)+offset, letterWidth, letterHeight);
      theLetters.add(myLetter);
    }
  }

  //GUI
  cp5 = new ControlP5(this);
  int activeColor = color(0, 130, 164);
  //cp5.setAutoDraw(true);
  cp5.setColorActive(activeColor);
  cp5.setColorBackground(color(170));
  cp5.setColorForeground(color(50));
  cp5.setColorCaptionLabel(color(255));
  cp5.setColorValueLabel(color(255));


  // ScollList sélection port série
  List l = Arrays.asList(Serial.list());
  cp5.addScrollableList("Serial_RS485")
    .setPosition(offset, offset+letterHeight*rows+interlignage*(rows+2))
    .setSize(150, 120)
    .setItemHeight(20)
    .setBarHeight(15)
    .addItems(l)
    ;

  // Connection 
  cp5.addBang("connectSerial")
    .setPosition(offset+165, offset+letterHeight*rows+interlignage*(rows+2))
    .setSize(55, 15)
    .setCaptionLabel("Connect RS485")
    ;
  /*  
   cp5.addBang("recheckConnectSerial")
   .setPosition(offset+165, offset+letterHeight*rows+interlignage*(rows+5))
   .setSize(55, 15)
   .setCaptionLabel("Check Serial")
   ;
   */
  // Stockage
  cp5.addBang("enregistrerMessage")//offsetXStock-20, offsetYStock-40
    .setPosition(width/2+105, offsetYStock-55)
    .setSize(70, 15)
    .setCaptionLabel("Sauver Message")

    ;

  // Précédent
  cp5.addBang("precedent")
    .setPosition(width/2+20, height -offset)
    .setSize(55, 15)
    .setCaptionLabel("<---")
    ;

  // Suivant
  cp5.addBang("suivant")
    .setPosition(width/2+95, height -offset)
    .setSize(55, 15)
    .setCaptionLabel("--->")
    ;

  //Charger
  cp5.addBang("charger")
    .setPosition(width/2-75, height -offset)
    .setSize(55, 15)
    .setCaptionLabel("Charger")
    ;

  //Supprimer
  cp5.addBang("supprimer")
    .setPosition(width/2-150, height -offset)
    .setSize(55, 15)
    .setCaptionLabel("Supprimer")
    ;

  // Affichage 
  cp5.addBang("Afficher")
    .setPosition(width /2-50, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(100, 100)
    .setColorForeground(color(125, 0, 00))
    .setCaptionLabel("Affichage Public")
    ;

  // Affichage 
  cp5.addBang("AfficherShuffle")
    .setPosition(width /2+100, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(100, 100)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("Affichage Public Melange")
    ;

  // Affichage 
  cp5.addBang("NOIR")
    .setPosition(width /2-200, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(100, 100)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("Noir Public ")
    ;


  //Infos
  myTextlabelA = new Textlabel(cp5, "[F1] Sauver Fichier       [F2] Ouvrir Fichier       [$] pour le carre blanc", 10, 10, 400, 200);

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  //Setup
  offsetXCaractereSetup = offset;
  offsetYCaractereSetup = offset+letterHeight*rows+interlignage*(rows+11); 

  /////////////////////////////////////////////////////////////
  //CORRECTION FACTOR
  cp5.addBang("moins1")
    .setPosition(offsetXCaractereSetup+100, offsetYCaractereSetup+48)
    .setSize(15, 15)
    .setCaptionLabel("-")
    ;

  cp5.addBang("plus1")
    .setPosition(offsetXCaractereSetup+125, offsetYCaractereSetup+48)
    .setSize(15, 15)
    .setCaptionLabel("+")
    ;

  cp5.addBang("moins2")
    .setPosition(offsetXCaractereSetup+100, offsetYCaractereSetup+88)
    .setSize(15, 15)
    .setCaptionLabel("-")
    ;

  cp5.addBang("plus2")
    .setPosition(offsetXCaractereSetup+125, offsetYCaractereSetup+88)
    .setSize(15, 15)
    .setCaptionLabel("+")
    ;
  /////////////////////////////////////////////////////////////
  //Boutons spécifiques à chaque caractère
  // Renvoyer carcatère 
  cp5.addBang("CaractereReset")
    .setPosition(offsetXCaractereSetup+10, offsetYCaractereSetup+128)
    .setSize(55, 15)
    .setCaptionLabel("Reset Module")
    ;

  cp5.addBang("CaractereForward")
    .setPosition(offsetXCaractereSetup+100, offsetYCaractereSetup+128)
    .setSize(55, 15)
    .setCaptionLabel("Avance Manuelle Module")
    ;

  cp5.addBang("CaractereAfficher")
    .setPosition(offsetXCaractereSetup+10, offsetYCaractereSetup+188)
    .setSize(150, 15)
    .setCaptionLabel("Relancer Module")
    .setColorForeground(color(125))
    ;


  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // Réseau de boutons pour lancer par colonne et par ligne

  for (int i = 0; i<cols; i++)
  {
    cp5.addBang("Col"+i)
      .setPosition(i*letterWidth+offset+letterWidth/2-7, offset-40)
      .setSize(15, 15);
  }

  for (int j = 0; j<rows; j++)
  {
    cp5.addBang("Ligne"+j)
      .setPosition(offset - 40, j*letterHeight+offset+letterHeight/2-7+interlignage*j)
      .setSize(15, 15);
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // Boutons pour envois "transition" par colonne ou par ligne

  // Affichage 
  cp5.addBang("COL0TO20")
    .setPosition(width /2+280, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(50, 30)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("Col ->")
    ;

  cp5.addBang("COL20TO0")
    .setPosition(width /2+380, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(50, 30)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("Col <-")
    ;

  cp5.addBang("COLMIDDLE")
    .setPosition(width /2+480, offset+letterHeight*rows+interlignage*(rows+1))
    .setSize(50, 030)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("Col < | >")
    ;

  cp5.addBang("LINETOPBOTTOM")
    .setPosition(width /2+280, offset+letterHeight*rows+interlignage*(rows+5))
    .setSize(50, 30)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("LIGNE 1 2 3 ") 
    ;

  cp5.addBang("LINEBOTTOMTOP")
    .setPosition(width /2+380, offset+letterHeight*rows+interlignage*(rows+5))
    .setSize(50, 30)
    .setColorForeground(color(0, 40, 70))
    .setCaptionLabel("LIGNE 3 2 1")
    ;




  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  // Jeu avec les parametres d'envoi
  cp5.addSlider("sliderValue")
    .setPosition(width - offset - 250, height - 250)
    .setSize(200, 15)
    .setRange(50, 1000)
    .setCaptionLabel("Delai envoi en ms")
    .setColorBackground(color(20))
    ;

  cp5.addSlider("multiplicateur")
    .setPosition(width - offset - 250, height - 225)
    .setRange(1, 15)
    .setSize(200, 15)
    .setNumberOfTickMarks(15)
    .setCaptionLabel("multiplicateur")
    .setColorBackground(color(20))
    ;


  // create a toggle and change the default look to a (on/off) switch look
  /*
  cp5.addToggle("toggleValue")
   .setPosition(width - offset - 190, height - 120)
   .setSize(50, 20)
   .setValue(false)
   .setCaptionLabel("Envoi melange")
   ;
   */
  shuffleIndex = new IntList();
  for (int i = 0; i<rows*cols; i++)
  {
    shuffleIndex.append(i);
  }

  loadData("data.json");

  paramsValues = loadJSONArray("params.json");
  println("Paramètres de correction stockés");
  for (int i = 0; i < paramsValues.size(); i++) {

    JSONObject module = paramsValues.getJSONObject(i); 

    int id = module.getInt("id");
    int corr1 = module.getInt("corr1");
    int corr2 = module.getInt("corr2");

    println(id + ", " + corr1 + ", " + corr2);

    // Application à chaque caractère
    theLetters.get(id).setCorrectionFactor1(corr1);
    theLetters.get(id).setCorrectionFactor2(corr2);
  }
}  

void draw()
{
  background(0);
  myTextlabelA.draw(this); 

  // On les dessine à l'écran
  for (int i = 0; i< theLetters.size(); i++)
  {
    theLetters.get(i).display();
  }

  // Messages Stockés
  if (storedMessages.size()>0)
  {

    messageStock = storedMessages.get(currentIndexMessage);
    //On écrit les message stockés en plus petit
    for ( int y=0; y<rows; y++)
    {
      for (int x=0; x<cols; x++)
      {
        int XStock = offsetXStock + x*(letterWidth/3);
        int YStock = offsetYStock + y*letterHeight/3+(interlignage/3*y);
        fill (125);
        textFont(f, 18);
        textAlign(CENTER, CENTER);
        text(messageStock[x+(y*cols)], XStock, YStock);
      }
    }
    fill (255);
    textFont(f, 15);
    textAlign(LEFT, CENTER);

    String infosStocks = "Message n°"+(currentIndexMessage+1)+"/"+storedMessages.size();
    text(infosStocks, offsetXStock-20, offsetYStock-40);

    textFont(f, 13);
    String infosFile = "Dernier Fichier chargé: "+lastFileLoaded;
    text(infosFile, offsetXStock-20, offsetYStock-60);
  }

  stroke(255);
  noFill();
  rect(offsetXStock-20, offsetYStock-20, 360, 105);


  // On affiche les infos de chaque caractère, si l'un d'eux est sélectionné.
  noFill();
  stroke(255);
  rect(offsetXCaractereSetup, offsetYCaractereSetup-5, 220, 240);
  fill(255);
  textFont(f2, 12);
  textAlign(LEFT, CENTER);
  text("PARAMETRAGE MODULE", offsetXCaractereSetup+10, offsetYCaractereSetup+10);
  String module = "[  ]"; 
  String corr1 = "Correction 1: "; 
  String corr2 = "Correction 2: "; 

  if (indexSelected>-1)
  {
    Letter myLetter = theLetters.get(indexSelected);
    storedCorrectionFactor1 = myLetter.getCorrectionFactor1();
    storedCorrectionFactor2 = myLetter.getCorrectionFactor2();
    ////
    // Display everything

    fill(255);
    textFont(f2, 12);
    textAlign(LEFT, CENTER);
    module = "["+indexSelected+"]";
    corr1 = "Correction 1: "+ storedCorrectionFactor1;
    corr2 = "Correction 2: "+ storedCorrectionFactor2;
  }
  text(module, offsetXCaractereSetup+10, offsetYCaractereSetup+27);
  text(corr1, offsetXCaractereSetup+10, offsetYCaractereSetup+55);
  text(corr2, offsetXCaractereSetup+10, offsetYCaractereSetup+95);
}



void keyPressed()
{
  if (keyCode == java.awt.event.KeyEvent.VK_F1) {   
    selectOutput("Selectionner un fichier à enregistrer:", "fileSelectedOutput");
  }
  if (keyCode == java.awt.event.KeyEvent.VK_F2) { 
    selectInput("Selectionner un fichier à charger:", "fileSelectedInput");
  }
  if (key == CODED) {
    if (keyCode == LEFT && index>0)index--;
    else if (keyCode == RIGHT && index<62)index++;
  }
  if (key == BACKSPACE && index >0) {
    message[index-1] = ' ';
    index--;
  } 
  // checker si ça fait partie des caractères que l'on sait afficher
  else if (index<rows*cols && key != BACKSPACE && key != CODED && key != BACKSPACE && key != TAB && key != ENTER && key != RETURN && key != ESC && key != ENTER && key != DELETE && listCharacters.hasKey(str(key).toUpperCase()))
  {
    String myStr = str(key).toUpperCase();
    char myChar = myStr.charAt(0);
    message[index] = myChar;
    if (index<63)index++;
  }
  updateLetters();
}

void mousePressed() {
  if (mouseButton == RIGHT) {
    indexSelected=-1;
  }
}

void mouseReleased()
{
  if (mouseButton == LEFT) {
    // Si on est à l'intérieur du tableau
    if (mouseX>offset && mouseX<offset+(cols*letterWidth) && mouseY>offset && mouseY<offset + (rows*letterHeight))
    {
      for (int i = 0; i< theLetters.size(); i++)
      {
        if (theLetters.get(i).mouseOver())
        {
          unselectAll();
          theLetters.get(i).setSelected();
          indexSelected = i;
        }
      }
    }
  }
}

void unselectAll()
{
  for (int i = 0; i< theLetters.size(); i++)
  {
    theLetters.get(i).setUnselected();
  }
}
void updateLetters()
{
  for (int i = 0; i< theLetters.size(); i++)
  {
    theLetters.get(i).updateChar(message[i]);
  }
}

//////// GUI ///////////

void Serial_RS485(int n)
{
  portName = (String)cp5.get(ScrollableList.class, "Serial_RS485").getItem(n).get("text");
  myRS485port = n;
}

void connectSerial() {
  if (myRS485port > -1)
  {

    myPort485 = new Serial(this, portName, 9600);
    myPort485Created = true;
    println("Connection RS485 créée: " + myPort485);
  }
}

/*
void recheckConnectSerial()
 {
 
 
 }
 */


void Afficher() {
  int indexTemp;
  for (int i = 0; i< theLetters.size(); i++)
  {
    indexTemp = i;
    String myChar = theLetters.get(indexTemp).getChar();
    int myConvertedChar = listCharacters.get(myChar);
    int myCorrection1 = theLetters.get(indexTemp).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(indexTemp).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", indexTemp, myConvertedChar, myCorrection1, myCorrection2);
    System.out.print("Module ["+ indexTemp + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myPort485Created == true)
    {
      myPort485.write(myString);
    }
    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexTemp, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();
    delay(sliderValue*multiplicateur);
  }
}

void AfficherShuffle() {
  shuffleIndex.shuffle();
  int indexTemp;
  for (int i = 0; i< theLetters.size(); i++)
  {
    indexTemp = shuffleIndex.get(i);

    String myChar = theLetters.get(indexTemp).getChar();
    int myConvertedChar = listCharacters.get(myChar);
    int myCorrection1 = theLetters.get(indexTemp).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(indexTemp).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", indexTemp, myConvertedChar, myCorrection1, myCorrection2);
    System.out.print("Module ["+ indexTemp + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myPort485Created == true)
    {
      myPort485.write(myString);
    }
    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexTemp, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();
    delay(sliderValue*multiplicateur);
  }
}

void NOIR() {
  int indexTemp;
  for (int i = 0; i< theLetters.size(); i++)
  {
    indexTemp = i;
    String myChar = theLetters.get(indexTemp).getChar();
    int myConvertedChar = 46;
    int myCorrection1 = theLetters.get(indexTemp).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(indexTemp).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", indexTemp, myConvertedChar, myCorrection1, myCorrection2);
    System.out.print("Module ["+ indexTemp + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myPort485Created == true)
    {
      myPort485.write(myString);
    }
    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexTemp, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();

    delay(sliderValue*multiplicateur);
  }
}



void enregistrerMessage() {
  char[] messageStored = new char[63];
  messageStored = message.clone();
  storedMessages.add(messageStored);
  currentIndexMessage = storedMessages.size()-1;

  saveJson();
}


void precedent() {
  if (currentIndexMessage != 0)
  {
    currentIndexMessage--;
  }
  index = 0;
}

void suivant() {
  if (currentIndexMessage < storedMessages.size()-1)
  {
    currentIndexMessage++;
  }
  index = 0;
}

void charger() {
  if (storedMessages.size()>0)
  {
    message = storedMessages.get(currentIndexMessage).clone();
    updateLetters();
    index = 0;
  }
}

void supprimer() {

  storedMessages.remove(currentIndexMessage);
  if (currentIndexMessage == storedMessages.size())
  {
    currentIndexMessage--;
  }
  saveJson();
}

void sauvegarderliste() {
  selectOutput("Selectionner un fichier à enregistrer:", "fileSelectedOutput");
}


void fileSelectedOutput(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    saveJSONObject(json, selection.getAbsolutePath()+".json");
  }
}

void fileSelectedInput(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    storedMessages.clear();
    println("User selected " + selection.getAbsolutePath());
    currentIndexMessage = 0;
    lastFileLoaded = selection.getName();
    loadData(selection.getAbsolutePath());
    saveJson();
  }
}

////////////////////////////////////////////////////////////////////////////////////////
//Fonctions de parametrage individuels de chaque module
void moins1()
{
  if (indexSelected>-1)
  {
    Letter myLetter = theLetters.get(indexSelected);
    if (storedCorrectionFactor1>0)
    {
      myLetter.setCorrectionFactor1(storedCorrectionFactor1-1);
    }
  }
  saveParams();
}

void moins2()
{
  if (indexSelected>-1)
  {
    Letter myLetter = theLetters.get(indexSelected);
    if (storedCorrectionFactor2>0)
    {
      myLetter.setCorrectionFactor2(storedCorrectionFactor2-1);
    }
  }
  saveParams();
}

void plus1()
{
  if (indexSelected>-1)
  {
    Letter myLetter = theLetters.get(indexSelected);
    myLetter.setCorrectionFactor1(storedCorrectionFactor1+1);
  }
  saveParams();
}

void plus2()
{
  if (indexSelected>-1)
  {
    Letter myLetter = theLetters.get(indexSelected);
    myLetter.setCorrectionFactor2(storedCorrectionFactor2+1);
  }
  saveParams();
}

void CaractereReset()
{
  String myChar = theLetters.get(indexSelected).getChar();
  int myConvertedChar = 666;
  int myCorrection1 = theLetters.get(indexSelected).getCorrectionFactor1();
  int myCorrection2 = theLetters.get(indexSelected).getCorrectionFactor2();
  String myString = String.format("S%02d%03d%02d%02dE", indexSelected, myConvertedChar, myCorrection1, myCorrection2 );
  System.out.print("Module ["+ indexSelected + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
  if (myRS485port > -1)
  {
    myPort485.write(myString);
  }
  //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
  for (int j=0; j<nbreDeRetry; j++)
  {
    int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
    String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexSelected, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
    System.out.print(" SRC ["+myString2+"] ");
    if (myRS485port > -1)
    {
      myPort485.write(myString2);
    }
    delay(10);
  }
  System.out.println();

  delay(delaiBase);
}


void CaractereForward()
{
  String myChar = theLetters.get(indexSelected).getChar();
  int myConvertedChar = 333;
  int myCorrection1 = theLetters.get(indexSelected).getCorrectionFactor1();
  int myCorrection2 = theLetters.get(indexSelected).getCorrectionFactor2();
  String myString = String.format("S%02d%03d%02d%02dE", indexSelected, myConvertedChar, myCorrection1, myCorrection2 );
  System.out.print("Module ["+ indexSelected + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
  if (myRS485port > -1)
  {
    myPort485.write(myString);
  }
  //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
  for (int j=0; j<nbreDeRetry; j++)
  {
    int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
    String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexSelected, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
    System.out.print(" SRC ["+myString2+"] ");
    if (myRS485port > -1)
    {
      myPort485.write(myString2);
    }
    delay(10);
  }
  System.out.println();
  delay(delaiBase);
}

void CaractereAfficher()
{
  if (indexSelected>-1)
  {
    String myChar = theLetters.get(indexSelected).getChar();
    int myConvertedChar = listCharacters.get(myChar);
    int myCorrection1 = theLetters.get(indexSelected).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(indexSelected).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", indexSelected, myConvertedChar, myCorrection1, myCorrection2 );
    System.out.print("Module ["+ indexSelected + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myRS485port > -1)
    {
      myPort485.write(myString);
    }
    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = (indexSelected+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", indexSelected, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();
    delay(delaiBase);
  }
}


////////////////////////////////////////////////////////////////////////////////////////



void loadData(String myJson) {
  // Load JSON file
  // Temporary full path until path problem resolved.
  json = loadJSONObject(myJson);

  JSONArray messageData = json.getJSONArray("messages");

  for (int i = 0; i < messageData.size(); i++) {
    // Get each object in the array
    JSONObject myMessage = messageData.getJSONObject(i); 
    String theChars = myMessage.getString("text");

    // Put object in array
    storedMessages.add(theChars.toCharArray());
    //println(storedMessages.get(i));
  }
}

void saveParams()
{
  JSONArray savedParamsValues = new JSONArray(); 

  for (int i = 0; i< theLetters.size(); i++)
  {

    JSONObject module = new JSONObject();

    module.setInt("id", i);
    module.setInt("corr1", theLetters.get(i).getCorrectionFactor1());
    module.setInt("corr2", theLetters.get(i).getCorrectionFactor2());

    savedParamsValues.setJSONObject(i, module);
  }
  saveJSONArray(savedParamsValues, "data/params.json");
}

void saveJson()
{
  JSONArray messageData = json.getJSONArray("messages");
  // empty it
  println(messageData.size());
  while (messageData.size()>0)
  {
    messageData.remove(messageData.size()-1);
  }
  println(messageData.size());
  for (int i =0; i<storedMessages.size(); i++)
  {
    char[] myMessage =  storedMessages.get(i);

    // Create a new JSON object
    JSONObject newMessage = new JSONObject();
    newMessage.setString("text", String.valueOf(myMessage));

    messageData.append(newMessage);
  }
  // Save new data
  saveJSONObject(json, "data/data.json");
  //loadData();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Affichage 
void COL0TO20 ()
{
  for (int i=0; i<21; i++)
  {
    sendCol(i) ;
    delay(sliderValue*multiplicateur);
  }
  delay(delaiBase);
}


void COL20TO0 ()
{
  for (int i=20; i>-1; i--)
  {
    sendCol(i) ;
    delay(sliderValue*multiplicateur);
  }
  delay(delaiBase);
}


void COLMIDDLE()
{
  int middle = 10;
  sendCol(middle) ;

  for (int i = 0; i<11; i++)
  {

    sendCol(middle-i) ;
    delay(delaiBase);
    sendCol(middle+i) ;
    delay(sliderValue*multiplicateur);
  }
  delay(delaiBase);
}

void LINETOPBOTTOM()
{
  sendLignes(0);
  delay(sliderValue*multiplicateur);
  sendLignes(1);
  delay(sliderValue*multiplicateur);
  sendLignes(2);
  delay(delaiBase);
}

void LINEBOTTOMTOP()
{
  sendLignes(2);
  delay(sliderValue*multiplicateur);
  sendLignes(1);
  delay(sliderValue*multiplicateur);
  sendLignes(0);
  delay(delaiBase);
}





// Colonnes et lignes
void sendLignes(int index1)
{
  for (int i=0+(index1*cols); i<cols+(index1*cols); i++)
  {

    String myChar = theLetters.get(i).getChar();
    int myConvertedChar = listCharacters.get(myChar);
    int myCorrection1 = theLetters.get(i).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(i).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", i, myConvertedChar, myCorrection1, myCorrection2 );
    System.out.print("Module ["+ i + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myRS485port > -1)
    {
      myPort485.write(myString);
    }

    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = (i+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", i, myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();

    delay(delaiBase);
  }
}

void Ligne0()
{
  println("Ligne0");
  sendLignes(0) ;
}

void Ligne1()
{
  println("Ligne1");
  sendLignes(1) ;
}

void Ligne2()
{
  println("Ligne2");
  sendLignes(2) ;
}

void sendCol(int index1)
{

  for (int i=0; i<rows; i++)
  {

    String myChar = theLetters.get(index1+(i*cols)).getChar();
    int myConvertedChar = listCharacters.get(myChar);
    int myCorrection1 = theLetters.get(index1+(i*cols)).getCorrectionFactor1();
    int myCorrection2 = theLetters.get(index1+(i*cols)).getCorrectionFactor2();
    String myString = String.format("S%02d%03d%02d%02dE", index1+(i*cols), myConvertedChar, myCorrection1, myCorrection2 );
    System.out.print("Module ["+ (index1+(i*cols)) + "] - Caractere ["+ myChar + "] - Message envoyé: "+  myString);
    if (myRS485port > -1)
    {
      myPort485.write(myString);
    }
    //     T<ID (2)><lettre(3)><cor1(2)><Cor2(2)><numero message(1)><somme de tt les chiffres modulo10(1)>F 
    for (int j=0; j<nbreDeRetry; j++)
    {
      int mySRC = ((index1+(i*cols))+myConvertedChar+myCorrection1+ myCorrection2+ j)%10;
      String myString2 = String.format("T%02d%03d%02d%02d%01d%01dF", index1+(i*cols), myConvertedChar, myCorrection1, myCorrection2, j, mySRC);
      System.out.print(" SRC ["+myString2+"] ");
      if (myRS485port > -1)
      {
        myPort485.write(myString2);
      }
      delay(10);
    }
    System.out.println();
    delay(delaiBase);
  }
}

void Col0()
{
  println("Col0");
  sendCol(0) ;
}

void Col1()
{
  println("Col1");
  sendCol(1) ;
}

void Col2()
{
  println("Col2");
  sendCol(2) ;
}

void Col3()
{
  println("Col3");
  sendCol(3) ;
}

void Col4()
{
  println("Col4");
  sendCol(4) ;
}

void Col5()
{
  println("Col5");
  sendCol(5) ;
}

void Col6()
{
  println("Col6");
  sendCol(6) ;
}

void Col7()
{
  println("Col7");
  sendCol(7) ;
}

void Col8()
{
  println("Col8");
  sendCol(8) ;
}

void Col9()
{
  println("Col9");
  sendCol(9) ;
}

void Col10()
{
  println("Col10");
  sendCol(10) ;
}

void Col11()
{
  println("Col11");
  sendCol(11) ;
}

void Col12()
{
  println("Col12");
  sendCol(12) ;
}

void Col13()
{
  println("Col13");
  sendCol(13) ;
}

void Col14()
{
  println("Col14");
  sendCol(14) ;
}

void Col15()
{
  println("Col15");
  sendCol(15) ;
}

void Col16()
{
  println("Col16");
  sendCol(16) ;
}

void Col17()
{
  println("Col17");
  sendCol(17) ;
}

void Col18()
{
  println("Col18");
  sendCol(18) ;
}

void Col19()
{
  println("Col19");
  sendCol(19) ;
}

void Col20()
{
  println("Col20");
  sendCol(20) ;
}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void populateDict()
{
  listCharacters.set("2", 0); 
  listCharacters.set("3", 1); 
  listCharacters.set("4", 2); 
  listCharacters.set("5", 3); 
  listCharacters.set("6", 4);   
  listCharacters.set("7", 5); 
  listCharacters.set("8", 6); 
  listCharacters.set("9", 7); 
  listCharacters.set("(", 8); 
  listCharacters.set(")", 9); 
  listCharacters.set("'", 10); 
  listCharacters.set("/", 11); 
  listCharacters.set("?", 12); 
  listCharacters.set("!", 13); 
  listCharacters.set(":", 14); 
  listCharacters.set("+", 15);   
  listCharacters.set("-", 16); 
  listCharacters.set("&", 17); 
  listCharacters.set("<", 18); 
  listCharacters.set(">", 19);
  listCharacters.set("A", 20); 
  listCharacters.set("B", 21); 
  listCharacters.set("C", 22); 
  listCharacters.set("D", 23); 
  listCharacters.set("E", 24); 
  listCharacters.set("F", 25); 
  listCharacters.set("G", 26); 
  listCharacters.set("H", 27); 
  listCharacters.set("I", 28); 
  listCharacters.set("J", 29); 
  listCharacters.set("K", 30); 
  listCharacters.set("L", 31); 
  listCharacters.set("M", 32); 
  listCharacters.set("N", 33); 
  listCharacters.set("O", 34); 
  listCharacters.set("P", 35); 
  listCharacters.set("Q", 36); 
  listCharacters.set("R", 37); 
  listCharacters.set("S", 38); 
  listCharacters.set("T", 39); 
  listCharacters.set("U", 40); 
  listCharacters.set("V", 41);   
  listCharacters.set("W", 42); 
  listCharacters.set("X", 43); 
  listCharacters.set("Y", 44); 
  listCharacters.set("Z", 45);
  listCharacters.set(" ", 46);
  listCharacters.set("$", 47);
  listCharacters.set(".", 48); 
  listCharacters.set("1", 49);
}
