class Letter
{
  int letterX;
  int letterY;
  int letterWidth;
  int letterHeight;
  int id;
  char myChar = ' ';
  boolean over = false;
  boolean selected = false;
  boolean sent = false;

  //Correction de l'inertie du moteur
  int correctionFactor1 = 0;
  int correctionFactor2 = 0;

  public Letter(int _id, int _letterX, int _letterY, int _letterWidth, int _letterHeight)
  {
    letterX = _letterX;
    letterY = _letterY;
    letterWidth = _letterWidth;
    letterHeight = _letterHeight;
    id = _id;
  }

  public void display()
  {
    


    if (id == 10 || id == 31 || id == 52) 
    {
      strokeWeight(4);
      stroke(255);
    } else
    {
      strokeWeight(1);
      stroke(125);
    }
    

    
    
    if (mouseX>letterX && mouseX<letterX+letterWidth && mouseY>letterY && mouseY<letterY+letterHeight && index == id) {
      fill(100, 50, 50);
      over = true;
    } else if (mouseX>letterX && mouseX<letterX+letterWidth && mouseY>letterY && mouseY<letterY+letterHeight) {
      fill(100, 0, 0);
      over = true;
    } else if (index == id /* && second()%2 == 0*/)
    {
      fill(125);
      over = false;
    } else {
      fill(25);
      over = false;
    }
    if (selected)
    {
      fill(100, 0, 0);
    }
    rect(letterX+1, letterY+1, letterWidth-2, letterHeight-2);   
    fill (100);
    textFont(f2, 12);
    textAlign(LEFT, CENTER);
    text(id, letterX+5, letterY+12);
    fill (255);
    if (myChar != '$')
    {
      textFont(f, 48);
      textAlign(CENTER, CENTER);
      text(myChar, letterX+letterWidth/2, letterY+letterHeight/2);
    } else {
      fill(255);
      rect(letterX+10, letterY+10, letterWidth-20, letterHeight-20);
    }
    
    
    if (id == 4 || id == 25 || id == 46 || id == 15 || id == 36 || id == 57)
    {
     stroke(255);
     strokeWeight(3);
     line(letterX+letterWidth, letterY+2, letterX+letterWidth, letterY+letterHeight-4);
    } else if (id == 6 || id == 27 || id == 48 || id == 13 || id == 34 || id == 55)
    {
      stroke(255,0,0);
     strokeWeight(3);
     line(letterX+letterWidth, letterY+2, letterX+letterWidth, letterY+letterHeight-4);
      
    }
  }

  public void updateChar(char _myChar)
  {
    myChar = _myChar;
  }

  public boolean mouseOver()
  {
    return over;
  }

  public String getChar()
  {
    return str(myChar);
  }

  public void setSelected()
  {
    selected = true;
  }

  public void setUnselected()
  {
    selected = false;
  }

  public void setCorrectionFactor1(int corr)
  {
    correctionFactor1 = corr;
  }

  public void setCorrectionFactor2(int corr)
  {
    correctionFactor2 = corr;
  }

  public int getCorrectionFactor1()
  {
    return correctionFactor1;
  }

  public int getCorrectionFactor2()
  {
    return correctionFactor2;
  }
}
