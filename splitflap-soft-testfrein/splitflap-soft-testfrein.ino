#define fadePin 10
#define brakePin 2
#define fadePinPlus 12

int state = 0;


void setup() {
  Serial.begin(57600);
  pinMode(fadePin, OUTPUT);
  pinMode(brakePin, OUTPUT);
  pinMode(fadePinPlus, OUTPUT);
  
  digitalWrite(fadePin, HIGH);
  digitalWrite(brakePin, LOW);
  digitalWrite(fadePinPlus, LOW);
  

}

void loop() {
  
  digitalWrite(fadePin, HIGH);
  digitalWrite(brakePin, LOW);
  digitalWrite(fadePinPlus, LOW);
  delay(3000);
  digitalWrite(fadePin, LOW);
  digitalWrite(brakePin, LOW);
  digitalWrite(fadePinPlus, LOW);
  delay(3000);
  
  digitalWrite(fadePin, HIGH);
  digitalWrite(brakePin, LOW);
  digitalWrite(fadePinPlus, LOW);
  delay(3000);
  digitalWrite(fadePinPlus, HIGH);
  delay(15);
  digitalWrite(fadePin, HIGH);
  digitalWrite(brakePin, HIGH);
  delay(100);
  digitalWrite(fadePin, LOW);
  digitalWrite(brakePin, LOW);
  digitalWrite(fadePinPlus, LOW);
  delay(3000);
  
  /*

  switch (state)
  {
    case 0:
      // On cherche le zero, en calculant le temps entre les intervalles
      digitalWrite(brakePin, LOW);
      analogWrite(fadePin, 255);
      digitalWrite(fadePinPlus, HIGH);
      delay(4000);
      state = 10;
      break;

    case 10:
      digitalWrite(fadePinPlus, LOW);
      digitalWrite(fadePin, HIGH);
      digitalWrite(brakePin, HIGH);
      delay(10000);
 
      state = 20;

      break;

    case 20:
      digitalWrite(fadePin, LOW);
      digitalWrite(brakePin, LOW);
      digitalWrite(fadePinPlus, LOW);

      break;

  }
  */

  delay(1);

}
