#define fadePin 10
#define brakePin 13
#define fadePinPlus 12
#define fourcheOpto 3

#include <SoftwareSerial.h>

int id = 0;
volatile bool encochePresente = false;
volatile unsigned long dureeEntreEncoches = 0;
volatile unsigned long lastTime = 0;

bool moteurTourne = false;

SoftwareSerial mySerial(2, 4); // RX, TX

// Lecture de l'ID de l'afficheur sur les microswitch
int lectureId() {
  const byte BinaryInput[] = {4, 5, 6, 7, 8, 9, 11};
  unsigned int binary = 0;
  for (byte x = 0; x < 7; x++) {
    byte value = digitalRead(BinaryInput[x]);
    binary = binary + (value << x);
  }
  return binary;
}

void ISR_fourche() {
  if (lastTime == millis()) return; //moins d'1ms entre 2 INT, pb
  if (digitalRead(fourcheOpto) == HIGH) return; //si on déclenche quand on sort de la fente, on refuse l'INT, donc on ne garde que l'entrée dans la fente
  //Serial.println("in");
  //Serial.println(millis());
  dureeEntreEncoches = millis() - lastTime;
  encochePresente = true;
  lastTime = millis();
}

char charRX;
int CharCount = 0;
int ReadedId = 0;
int ReadedLetter = 0;
bool changeObjectif = false;
int newObjectif = 0;

void mySerialEvent() {
  while (mySerial.available() > 0) {
    // get the new byte:
    charRX = (char)mySerial.read();
    Serial.print(charRX);
    switch (charRX) {
      case 'S':
        CharCount = 1; // ca commence
        ReadedId = 0;
        ReadedLetter = 0;
        break;
      case 'E':
        if (CharCount>=5 && CharCount<=6) {
          if (ReadedId == id) {
            newObjectif = ReadedLetter; //c'est à moi qu'on cause... je chope le nouvel objectif
            changeObjectif = true;
          }
          Serial.print(ReadedId);
          Serial.print(" ");
          Serial.println(ReadedLetter);
        }
        CharCount--;
        break;
      default :
        // trame E12255S
        switch (CharCount) {
          case 1 :
            ReadedId += (charRX - '0') * 10;
            break;
          case 2 :
            ReadedId += (charRX - '0') * 1;
            break;
          case 3 :
            ReadedLetter += (charRX - '0') * 100;
            break;
          case 4 :
            ReadedLetter += (charRX - '0') * 10;
            break;
          case 5 :
            ReadedLetter += (charRX - '0') * 1;
            break;
          default :// a trop lu
            //digitalWrite(ledPin2,HIGH);
            CharCount = -1;
            break;
        }
        if (-1 != CharCount) {
          CharCount++;
        }
        break; // end default sur la position du char lu
    }
  }
}

#define VITESSE_LENTE 0
#define VITESSE_RAPIDE 1
#define RECHERCHE_ZERO 0
#define TEST_TEMPS_MEDIAN_ENCOCHES 1
#define ARRET 2
#define RECHERCHE_OBJECTIF 3
#define ATTENTE_RECEPTION_ORDRE 4

unsigned long dernierTemps = 0;
int vraiDureeEncoche = 0;
int choix_Vitesse = VITESSE_RAPIDE; //vitesse rapide pour commencer
int tDureeMoyenneEntreEncoches[2] = {255, 155};
int tDureeMiniEntreEncoches[2] = {28, 20};
int tVitesses[2] = {63, 128};
int dureeMoyenneEntreEncoches = 255;
int dureeMiniEntreEncoches = 28;
int fenteObjectif = 0;
int compteurFentes = 0;
int choix_action = RECHERCHE_ZERO;
int prochain_choix_action = ARRET;
bool phaseRecherche = false;

//inertie du moteur
float correctionFactor1 = 3;
float correctionFactor2 = 5;

byte tTempsFentes[60];

bool fente_zero = false;
int valTemp = 0;
int valMediane = 0;
int dureeBasseEncoche = 0;
int dureeHauteEncoche = 0;
int cumulDureeEncoche = 0;
int objectif = 49;
bool relance = false;
bool debutArret = false;

void setup() {
  Serial.begin(57600);
  //RS485
  mySerial.begin(57600);
  pinMode(fadePin, OUTPUT);
  pinMode(brakePin, OUTPUT);
  pinMode(fadePinPlus, OUTPUT);

  //Optique
  pinMode(3, INPUT_PULLUP);

  digitalWrite(fadePin, LOW);
  digitalWrite(brakePin, HIGH);
  digitalWrite(fadePinPlus, LOW);

  //DIP
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);

  id =  lectureId();
  Serial.println("My ID : " + String(id));

  //on déclenche l'interruption sur le le font montant de la fourche (entrée dans l'encoche)
  attachInterrupt(digitalPinToInterrupt(fourcheOpto), ISR_fourche, FALLING);

  moteurTourne = true;
  choix_Vitesse = VITESSE_RAPIDE;
  choix_action = RECHERCHE_ZERO;
  prochain_choix_action = TEST_TEMPS_MEDIAN_ENCOCHES;
  Serial.println("fin de setup !");
}

void loop() {
  //Serial.println("debut de loop !");
  if (encochePresente || relance) {
    //Serial.println("duree reelle:"+String(dureeEntreEncoches));

    if (dureeEntreEncoches > 280 && dureeEntreEncoches < 500 && moteurTourne) {
      fente_zero = true;
    } else fente_zero = false;

    switch (choix_action) {
      case ARRET:
        moteurTourne = false;
        break;

      case RECHERCHE_ZERO:
        moteurTourne = true;
        if (fente_zero) {
          //on a trouvé le zéro
          Serial.println("\r\nZéro trouvé !");
          choix_action = prochain_choix_action;
          compteurFentes = 0;
          //tTempsFentes[0]=dureeEntreEncoches;
          Serial.print("0:" + String(dureeEntreEncoches) + ",");
        } else Serial.println("duree reelle:" + String(dureeEntreEncoches));
        break;

      case TEST_TEMPS_MEDIAN_ENCOCHES:
        if (!fente_zero) {
          compteurFentes++;
          tTempsFentes[compteurFentes] = dureeEntreEncoches;
          Serial.print(String(compteurFentes) + ":" + String(tTempsFentes[compteurFentes]) + ",");
          if (tTempsFentes[compteurFentes] < 100) Serial.print(" ");
          //compteurFentes++;
        }
        else {
          //on a trouvé le zéro à nouveau
          Serial.println("\r\nZéro trouvé à nouveau!");
          //tri à bulle des valeurs trouvées pour calculer la médiane
          for (int i = compteurFentes - 2; i > 0; i--) {
            for (int j = 0; j < i; j++) {
              if (tTempsFentes[j] > tTempsFentes[j + 1]) {
                valTemp = tTempsFentes[j];
                tTempsFentes[j] = tTempsFentes[j + 1];
                tTempsFentes[j + 1] = valTemp;
              }
            }
          }
          //calcul de la valeur médiane et des 2 fourchettes haute et basse
          valMediane = tTempsFentes[(int) (compteurFentes / 2)];
          dureeBasseEncoche = (int) (0.70 * valMediane);
          dureeHauteEncoche = (int) (1.30 * valMediane);
          Serial.println("mediane:" + String(valMediane) + "  dureeBasse:" + String(dureeBasseEncoche) + "  dureeHaute:" + String(dureeHauteEncoche));


          compteurFentes = 0;
          //tTempsFentes[0]=dureeEntreEncoches;
          //Serial.print("0:"+String(tTempsFentes[0])+",");
          choix_action = RECHERCHE_OBJECTIF;
        }
        break;

      case RECHERCHE_OBJECTIF:
        if (objectif == 48 && compteurFentes == 47) {
          compteurFentes++;
          delay(valMediane);
        } else if (objectif == 49 && compteurFentes == 48) {
          compteurFentes++;
        }
        if (compteurFentes == objectif) {
          //AJUSTEMENT DES TEMPS SUIVANT LES MOTEURS
          // les dmei fentes
          if (objectif == 11 || objectif == 36){
            delay(valMediane/2*correctionFactor1);
          }else{
          delay(valMediane/4*correctionFactor2);
          }
          choix_action = ARRET;
          moteurTourne = false;
          Serial.println("Objectif atteint ! Stop !!!");
        }
        if (dureeEntreEncoches > dureeBasseEncoche && dureeEntreEncoches < dureeHauteEncoche && moteurTourne) { //encoche complète, on compte...
          compteurFentes++;
          cumulDureeEncoche = 0;
        }
        if (dureeEntreEncoches < dureeBasseEncoche && moteurTourne) { //encoche incomplète, on cumule...
          cumulDureeEncoche += dureeEntreEncoches;
        }
        if (cumulDureeEncoche > dureeBasseEncoche && dureeEntreEncoches < dureeHauteEncoche && moteurTourne) { //encoche finalement complète
          compteurFentes++;
          cumulDureeEncoche = 0;
        }
        Serial.println(String(compteurFentes) + " [" + String(dureeEntreEncoches));

        break;

      default:
        break;
    }


    encochePresente = false;
    relance = false;
  }
  if (changeObjectif) {
    objectif = newObjectif;
    /*
    choix_action = RECHERCHE_ZERO;
    prochain_choix_action = TEST_TEMPS_MEDIAN_ENCOCHES;
    */
    
        choix_action=RECHERCHE_ZERO; //!moteurTourne;
        prochain_choix_action=RECHERCHE_OBJECTIF;
    
    Serial.println("On cherche le nouvel objectif...");
    changeObjectif = false;
    relance = true;
  }

  //Serial.println("choix:"+String(choix_action));
  if (moteurTourne) {
    digitalWrite(brakePin, HIGH);
    analogWrite(fadePin, tVitesses[choix_Vitesse]);
  }
  else {
    analogWrite(fadePin, 0);
    digitalWrite(brakePin, LOW);
  }

  //RS485
  if (mySerial.available()) {
    mySerialEvent();
  }
}
